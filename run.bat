cls
@echo off
REM set the class path,
REM assumes the build was executed with maven copy-dependencies
SET DEMO_CP=target\DEMO_ORM-1.0-SNAPSHOT.jar;target\dependency\*;

REM call the java VM, e.g, 
java -cp %DEMO_CP% demo_ORM.apresentacao.DemoORM -bootstrap:demo -smoke:basic