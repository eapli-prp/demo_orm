package demo_ORM.persistencia;

import demo_ORM.dominio.CondutorAutorizado;

public class CondutorAutorizadoRepositorioJPAImpl extends JpaRepository<CondutorAutorizado,Long> {

    protected String persistenceUnitName() {
        return "thePU";
    }
}
