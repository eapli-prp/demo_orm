package demo_ORM.persistencia;

import demo_ORM.dominio.ContratoAluguer;

public class ContratoRepositorioJPAImpl extends JpaRepository<ContratoAluguer,Long> {

    protected String persistenceUnitName() {
        return "thePU";
    }
}
