package demo_ORM.persistencia;

import demo_ORM.dominio.Cliente;

public class ClienteRepositorioJPAImpl extends JpaRepository<Cliente,Long> {

    protected String persistenceUnitName() {
        return "thePU";
    }
}