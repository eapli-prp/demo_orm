package demo_ORM.persistencia;

import demo_ORM.dominio.Automovel;

public class AutomovelRepositorioJPAImpl extends JpaRepository<Automovel,String> {

    protected String persistenceUnitName() {
        return "thePU";
    }
}
