package demo_ORM.aplicacao;

import demo_ORM.dominio.Cliente;
import demo_ORM.dominio.ContratoAluguer;
import demo_ORM.persistencia.ClienteRepositorioJPAImpl;

import java.util.List;

public class ClienteController {
    ClienteRepositorioJPAImpl RepoCliente = new ClienteRepositorioJPAImpl();

    public Cliente registarCliente(long cc, String nome){
        Cliente newCliente = new Cliente(cc,nome);
        return RepoCliente.add(newCliente);
    }

    public List<Cliente> listarClientes(){
        return RepoCliente.findAll();
    }

    public Cliente procuraCliente(long numCC){
        return RepoCliente.findById(numCC);
    }

    public Cliente acrescentarContrato(long cc, ContratoAluguer contrato){
        Cliente cliente = RepoCliente.findById(cc);
        cliente.addContrato(contrato);
        try {
            RepoCliente.update(cliente);
            return cliente;
        }
        catch(Exception ex)
        {
            System.out.println("ERRO NA AFETACAO DO CONTRATO AO CLIENTE");
            return null;
        }
    }
}

