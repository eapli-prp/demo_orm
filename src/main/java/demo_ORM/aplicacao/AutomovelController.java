package demo_ORM.aplicacao;

import demo_ORM.dominio.Automovel;
import demo_ORM.dominio.GrupoAutomovel;
import demo_ORM.persistencia.AutomovelRepositorioJPAImpl;

import java.util.List;

public class AutomovelController {

    AutomovelRepositorioJPAImpl RepoAuto = new AutomovelRepositorioJPAImpl();

    public Automovel registarAutomovel(String matricula, int kms, GrupoAutomovel grupoAutomovel){
        Automovel newAuto = new Automovel(kms, matricula,grupoAutomovel);
        return RepoAuto.add(newAuto);
    }

    public List<Automovel> listarAutomoveis(){
        return RepoAuto.findAll();
    }

    public Automovel procurarAutomovel(String matricula) {
        return RepoAuto.findById(matricula);
    }
}
