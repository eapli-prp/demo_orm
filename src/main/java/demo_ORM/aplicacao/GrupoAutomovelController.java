package demo_ORM.aplicacao;

import demo_ORM.dominio.GrupoAutomovel;
import demo_ORM.persistencia.GrupoAutomovelRepositorioJPAImpl;

import java.util.List;

public class GrupoAutomovelController {

    GrupoAutomovelRepositorioJPAImpl RepoGA = new GrupoAutomovelRepositorioJPAImpl();

    public GrupoAutomovel registarGrupoAutomovel(String classe, int portas) {
        GrupoAutomovel newGA = new GrupoAutomovel(classe, portas);
        return RepoGA.add(newGA);
    }

    public List<GrupoAutomovel> listarGruposAutomoveis() {
        return RepoGA.findAll();
    }

    public GrupoAutomovel procurarGrupoAutomovel(long id) {
        return RepoGA.findById(id);
    }
}
