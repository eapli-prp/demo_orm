package demo_ORM.aplicacao;

import demo_ORM.dominio.CondutorAutorizado;
import demo_ORM.persistencia.CondutorAutorizadoRepositorioJPAImpl;

import java.util.List;

public class CondutorAutorizadoController {
    CondutorAutorizadoRepositorioJPAImpl RepoCondutor= new CondutorAutorizadoRepositorioJPAImpl();

    public CondutorAutorizado registarCondutorAutorizado(long cartaConducao, String nome){
        CondutorAutorizado newCondutor = new CondutorAutorizado(cartaConducao,nome);
        return RepoCondutor.add(newCondutor);
    }

    public List<CondutorAutorizado> listarCondutoresAutorizados(){
        return RepoCondutor.findAll();
    }

    public CondutorAutorizado procuraCondutoresAutorizado(long cartaConducao){
        return RepoCondutor.findById(cartaConducao);
    }
}

