package demo_ORM.aplicacao;

import demo_ORM.dominio.Automovel;
import demo_ORM.dominio.CondutorAutorizado;
import demo_ORM.dominio.ContratoAluguer;
import demo_ORM.persistencia.ContratoRepositorioJPAImpl;

import java.util.List;


public class ContratoController {
    ContratoRepositorioJPAImpl ContratoAluguerRepoContratos = new ContratoRepositorioJPAImpl();

    public ContratoAluguer registarContrato(Automovel automovel, CondutorAutorizado condutorAutorizado){
        ContratoAluguer newContrato = new ContratoAluguer(automovel, condutorAutorizado);
        return ContratoAluguerRepoContratos.add(newContrato);
    }

    public List<ContratoAluguer> listarContratosAluguer(){
        return ContratoAluguerRepoContratos.findAll();
    }

    public ContratoAluguer procurarContratoAluguerl(long id) {
        return ContratoAluguerRepoContratos.findById(id);
    }
}