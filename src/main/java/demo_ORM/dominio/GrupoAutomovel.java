package demo_ORM.dominio;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class GrupoAutomovel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;
    private String classe;
    private int numPortas;

    public GrupoAutomovel() {
    }

    public GrupoAutomovel(String classe, int numPortas) {
        this.classe = classe;
        this.numPortas = numPortas;
    }

    public void NumPortas(int numPortas){
        this.numPortas=numPortas;
    }

    public int getNumPortas(){
        return this.numPortas;
    }

    public void Classe(String classe){
        this.classe=classe;
    }

    public String toString()
    {
        return("[GRUPO AUTOMOVEL : NumPortas = " + this.numPortas
                + " | Classe = " + this.classe + "]");
    }

    public String toMenuItem()
    {
        return("[" + this.Id + "] [NumPortas = "
                + this.numPortas
                + " | Classe = " + this.classe + "]");
    }
}
