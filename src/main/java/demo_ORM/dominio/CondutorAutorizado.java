package demo_ORM.dominio;

import javax.persistence.*;

@Entity
public class CondutorAutorizado {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long cartaConducao;
    private String nome;

    public CondutorAutorizado(){
    }

    public CondutorAutorizado(long cartaConducao, String nome){
        this.cartaConducao=cartaConducao;
        this.nome=nome;
    }

    public String toString(){
        return "[CONDUTOR : Carta de condução = "
                + this.cartaConducao
                + " | Nome = "
                + this.nome + "]";
    }

}
