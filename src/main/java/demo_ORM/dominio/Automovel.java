package demo_ORM.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Automovel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String matricula;
    private long kms;
    private GrupoAutomovel classe;

    public Automovel() {
    }

    public Automovel(long kms, String matricula, GrupoAutomovel classe) {
        this.kms = kms;
        this.matricula = matricula;
        this.classe = classe;
    }

    public void setKms(long kms){
        this.kms=kms;
    }

    public long getKms(){
        return this.kms;
    }

    public void setMatricula(String matricula){
        this.matricula = matricula;
    }

    public String Matricula(){
        return this.matricula;
    }

    public String toString(){
        return("[AUTOMOVEL : Matricula = "+ this.Matricula()
                + " | Kms = "+ this.kms
                + " | Classe = " + this.classe.toString() + "]");
    }
}
