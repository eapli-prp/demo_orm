package demo_ORM.dominio;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long numeroCC;
    private String nome;
    @OneToMany
    private List<ContratoAluguer> contratos;


    public Cliente(){
    }

    public Cliente(long cc, String nome){
        this.numeroCC=cc;
        this.nome=nome;
        this.contratos = new ArrayList<ContratoAluguer>();
    }

    public void addContrato(ContratoAluguer newContrato){
        this.contratos.add(newContrato);
    }

    @Override
    public String toString(){
        return "[CLIENTE : " + this.nome
                + " (" + this.numeroCC + ")]";
    }

}