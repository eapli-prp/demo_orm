package demo_ORM.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ContratoAluguer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Automovel automovel;
    private CondutorAutorizado condutorAutorizado;

    public ContratoAluguer() {
    }

    public ContratoAluguer(Automovel automovel, CondutorAutorizado condutorAutorizado) {
        this.automovel = automovel;
        this.condutorAutorizado = condutorAutorizado;
    }

    @Override
    public String toString() {
        return "ContratoAluguer{" +
                "id=" + id +
                ", automovel=" + automovel +
                ", condutorAutorizado=" + condutorAutorizado +
                '}';
    }
}
