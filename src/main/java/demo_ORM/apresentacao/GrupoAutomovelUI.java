package demo_ORM.apresentacao;


import demo_ORM.aplicacao.GrupoAutomovelController;
import demo_ORM.dominio.GrupoAutomovel;
import demo_ORM.util.Console;

import java.util.List;

/**
 *
 * @author mcn
 */
public class GrupoAutomovelUI {

    private final GrupoAutomovelController controller = new GrupoAutomovelController();

    public void registarGA() {
        System.out.println("*** Registo Grupo Autom�vel ***\n");
        String classe = Console.readLine("Classe:");
        int portas = Console.readInteger("N�mero de portas");
        GrupoAutomovel grupoAutomovel = controller.registarGrupoAutomovel(classe, portas);
        System.out.println("Grupo Automóvel" + grupoAutomovel.toString());
    }
    public void listarGAs() {
        System.out.println("*** Listar Grupos Autom�vel ***\n");
        List<GrupoAutomovel> allGa = controller.listarGruposAutomoveis();
        for (GrupoAutomovel ga:allGa) {
            System.out.println(ga.toString());
        }
        System.out.println("\n");
    }

    public void procurarGAPorID(long id) {
        throw new UnsupportedOperationException("Ainda n�o implementada.");
    }
}
