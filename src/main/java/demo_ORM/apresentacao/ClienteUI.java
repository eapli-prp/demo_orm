package demo_ORM.apresentacao;

import demo_ORM.aplicacao.ClienteController;
import demo_ORM.dominio.Cliente;
import demo_ORM.util.Console;

import java.util.List;

public class ClienteUI {
    private final ClienteController controller = new ClienteController();

    public void registarCliente() {
        System.out.println("*** Registo de Cliente ***\n");
        long cartaoCidadao = Long.parseLong(Console.readLine("N�mero do Cart�o de Cidad�o:"));
        String nome = Console.readLine("Nome:");

        Cliente cliente = controller.registarCliente(cartaoCidadao,nome);
        System.out.println("Cliente" + cliente.toString());
    }

    public void listarClientes() {
        System.out.println("*** Listar Clientes ***\n");
        List<Cliente> all = controller.listarClientes();
        for (Cliente c:all) {
            System.out.println(c.toString());
        }
        System.out.println("\n");
    }

    public void procurarClientePorCC(long cartaoCidadao) {
        throw new UnsupportedOperationException("Ainda n�o implementado.");
    }
}
