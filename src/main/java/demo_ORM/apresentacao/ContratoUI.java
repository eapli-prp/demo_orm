package demo_ORM.apresentacao;

import demo_ORM.aplicacao.*;
import demo_ORM.dominio.*;
import demo_ORM.util.Console;

import java.util.List;

public class ContratoUI {

    private final ContratoController controller = new ContratoController();

    public void registarContrato() {
        System.out.println("*** Registo de Contrato de Aluguer ***\n");

        System.out.println("Automoveis existentes:");
        AutomovelController aCtrl = new AutomovelController();
        List<Automovel> all = aCtrl.listarAutomoveis();
        for (Automovel a: all) {
            System.out.println(a.toString());
        }
        String matricula = Console.readLine("Escolha a Matricula:");

        CondutorAutorizado condutor = (new CondutorAutorizadoUI()).registarCondutorAutorizado();
        ContratoAluguer contrato = controller.registarContrato(aCtrl.procurarAutomovel(matricula),condutor);

        System.out.println("Clientes existentes:");
        ClienteController cliCtrl = new ClienteController();
        List<Cliente> allc = cliCtrl.listarClientes();
        for (Cliente c: allc) {
            System.out.println(c.toString());
        }
        long cc = Long.parseLong(Console.readLine("Escolha o Cliente:"));

        cliCtrl.acrescentarContrato(cc,contrato);

        System.out.println("Contrato" + contrato.toString());
    }

    public void listarContratos() {
        System.out.println("*** Listar Contratos ***\n");
        List<ContratoAluguer> all = controller.listarContratosAluguer();
        for (ContratoAluguer a:all) {
            System.out.println(a.toString());
        }
        System.out.println("\n");
    }

    public void procurarContatoPorId(long id) {
        throw new UnsupportedOperationException("Ainda n�o implementado.");
    }
}
