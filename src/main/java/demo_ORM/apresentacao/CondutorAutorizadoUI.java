package demo_ORM.apresentacao;

import demo_ORM.aplicacao.CondutorAutorizadoController;
import demo_ORM.dominio.CondutorAutorizado;
import demo_ORM.util.Console;

import java.util.List;

public class CondutorAutorizadoUI {
    private final CondutorAutorizadoController controller = new CondutorAutorizadoController();

    public CondutorAutorizado registarCondutorAutorizado() {
        System.out.println("*** Registo de Condutor Autorizado ***\n");
        long cartaConducao = Long.parseLong(Console.readLine("Número da Carta de Condução:"));
        String nome = Console.readLine("Nome:");

        return controller.registarCondutorAutorizado(cartaConducao,nome);
    }

    public void listarCondutoresAutorizados() {
        System.out.println("*** Listar Condutores Autorizados ***\n");
        List<CondutorAutorizado> all = controller.listarCondutoresAutorizados();
        for (CondutorAutorizado c:all) {
            System.out.println(c.toString());
        }
        System.out.println("\n");
    }

    public void procurarCondutorPorCartaConducao(long cartaConducao) {
        throw new UnsupportedOperationException("Ainda não implementado.");
    }
}