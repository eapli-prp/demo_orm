package demo_ORM.apresentacao;

import demo_ORM.aplicacao.AutomovelController;
import demo_ORM.aplicacao.GrupoAutomovelController;
import demo_ORM.dominio.Automovel;
import demo_ORM.dominio.GrupoAutomovel;
import demo_ORM.util.Console;

import java.util.List;

public class AutomovelUI {

    private final AutomovelController controller = new AutomovelController();

    public void registarAutomovel() {
        System.out.println("*** Registo de Autom�vel ***\n");
        String matricula = Console.readLine("Matricula:");
        int kms = Console.readInteger("N�mero de kilometros atuais:");

        System.out.println("Grupos automovel existentes:");
        GrupoAutomovelController gaCtrl = new GrupoAutomovelController();
        List<GrupoAutomovel> allGA = gaCtrl.listarGruposAutomoveis();
        for (GrupoAutomovel ga: allGA) {
            System.out.println(ga.toMenuItem());
        }
        int grupo = Console.readInteger("Escolha o grupo automovel pretendido:");

        Automovel automovel = controller.registarAutomovel(matricula, kms, gaCtrl.procurarGrupoAutomovel((long)grupo));
        System.out.println("Autom�vel" + automovel.toString());
    }

    public void listarAutomoveis() {
        System.out.println("*** Listar Autom�veis ***\n");
        List<Automovel> allAuto = controller.listarAutomoveis();
        for (Automovel a:allAuto) {
            System.out.println(a.toString());
        }
        System.out.println("\n");
    }

    public void procurarAutomovelPorMatricula(String matricula) {
        throw new UnsupportedOperationException("Ainda n�o implementado.");
    }


}
