/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo_ORM.apresentacao;

import demo_ORM.util.Console;

/**
 *
 * @author mcn
 */
public class MainMenu {
    public static void mainLoop() {
        int opcao = 0;
        do {
            opcao = menu();

            GrupoAutomovelUI GAUI = new GrupoAutomovelUI();
            AutomovelUI autoUI = new AutomovelUI();
            ClienteUI cliUI = new ClienteUI();
            ContratoUI contratoUI = new ContratoUI();

            switch (opcao) {
                case 0:
                    System.out.println("fim ...");
                    break;
                case 1:
                    GAUI.registarGA();
                    break;

                case 2:
                    GAUI.listarGAs();
                    break;

                case 3:
                    autoUI.registarAutomovel();
                    break;

                case 4:
                    autoUI.listarAutomoveis();
                    break;

                case 5:
                    cliUI.registarCliente();
                    break;

                case 6:
                    cliUI.listarClientes();
                    break;

                case 7:
                    contratoUI.registarContrato();
                    break;

                case 8:
                    System.out.printf("Funcionalidade n�o implementada");
                    break;

                default:
                    System.out.println("opcao n�o reconhecida.");
                    break;
            }
        } while (opcao != 0);

    }

    private static int menu() {
        int option = -1;
        System.out.println("");
        System.out.println("=============================");
        System.out.println(" Rent a Car ");
        System.out.println("=============================\n");
        System.out.println("1.Registar Grupo Autom�vel");
        System.out.println("2.Listar todos os Grupos Autom�veis");
        System.out.println("3.Registar Autom�vel");
        System.out.println("4.Listar todos os Autom�veis");
        System.out.println("5.Registar Cliente");
        System.out.println("6.Listar todos os Cliente");
        System.out.println("7.Registar Contrato de Aluguer");
        System.out.println("8.Listar todos os Contratos de Aluguer de um cliente");

        System.out.println("=============================");
        System.out.println("0. Sair\n\n");
        option = Console.readInteger("Por favor escolha uma op��o");
        return option;
    }
}
