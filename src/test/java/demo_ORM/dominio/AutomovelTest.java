package demo_ORM.dominio;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class AutomovelTest {
    @org.junit.Test
    public void setKms() {
        Automovel a = new Automovel(100,"dummy",new GrupoAutomovel());
        a.setKms(200);
        assertEquals(200, a.getKms());
    }

    @org.junit.Test
    public void matricula() {
        Automovel a = new Automovel(100,"12-34-AB", new GrupoAutomovel());
        String expResult = "12-34-AB";
        String result = a.Matricula();
        assertEquals(expResult, result);
    }

}