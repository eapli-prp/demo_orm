package demo_ORM.dominio;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class GrupoAutomovelTest {
    @org.junit.Test
    public void numPortas() {
        GrupoAutomovel ga = new GrupoAutomovel("dummy",2);
        int result = ga.getNumPortas();
        int expected = 2;
        assertEquals(expected,result);
    }

    @org.junit.Test
    public void getNumPortas() {
        GrupoAutomovel ga = new GrupoAutomovel("dummy",2);
        int result = ga.getNumPortas();
        int expected = 2;
        assertEquals(expected,result);
    }
}